export const redes_sociales = [
    {
        "nombre": "Facebook",
        "url": "https://www.facebook.com/",
    },
    {
        "nombre": "Twitter",
        "url": "https://twitter.com/",
    },
    {
        "nombre": "Instagram",
        "url": "https://www.instagram.com/",
    },
    {
        "nombre": "LinkedIn",
        "url": "https://www.linkedin.com/",
    },
    {
        "nombre": "GitHub",
        "url": "https://github.com/",
    },
    {
        "nombre": "StackOverflow",
        "url": "https://stackoverflow.com/",
    },
    {
        "nombre": "Medium",
        "url": "https://medium.com/",
    },
    {
        "nombre": "Reddit",
        "url": "https://www.reddit.com/",
    },
    {
        "nombre": "Tumblr",
        "url": "https://www.tumblr.com/",
    },
    {
        "nombre": "Youtube",
        "url": "https://www.youtube.com/",
    },
    {
        "nombre": "Vimeo",
        "url": "https://vimeo.com/",
    },
    {
        "nombre": "Goodreads",
        "url": "https://www.goodreads.com/",
    },
    {
        "nombre": "SoundCloud",
        "url": "https://soundcloud.com/",
    },
    {
        "nombre": "Spotify",
        "url": "https://open.spotify.com/",
    },
    {
        "nombre": "Twitch",
        "url": "https://www.twitch.tv/",
    },
    {
        "nombre": "Yahoo",
        "url": "https://www.yahoo.com/",
    },
    {
        "nombre": "Snapchat",
        "url": "https://www.snapchat.com/",
    },
    {
        "nombre": "TikTok",
        "url": "https://www.tiktok.com/",
    },
];
