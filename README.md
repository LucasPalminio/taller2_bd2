# Taller 2 - Base de datos 2

Se utilizara una base de datos documental, en particular MongoDB, luego  se creara una colección llamada redes_sociales que contiene el registros de 100 personas ficticias, para ello se creo un script que genera registros de personas que incluye un nombre, apellido, sexo y fecha de nacimiento generados al azar, cada persona tiene en su interior un subdocumento que corresponde a la información de su pais (nombre, codigo,capital, continente)

## Pre-requisitos

Tener instalado NodeJS, NPM (viene incluido con nodejs) y mongodb

## Pasos para ejecutar el script

```bash
sudo service mongod start
npm install
node script.js
```

## Exportación a JSON

```bash
mongoexport --db=test --collection=redes_sociales --out=usuarios.json --jsonArray --pretty
```

## Importando JSON a MongoDB
```bash
mongoimport usuarios.json -d test -c redes_sociales --drop --jsonArray
```
