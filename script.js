import { MongoClient } from "mongodb";
import { crearRegistros } from "./coleccionPersonas.js";

/*
    Ideas:
    - Buscar un estudio para hacer el taller
    - Asignar una red social al azar a cada persona
    - Asignar una dirección de correo electrónico a cada persona
    - Asignar nombre de usuario a cada persona
    - Asignar una contraseña al azar a cada persona
    - Filtrar los resultados (por ejemplo, según el continente), para guardar varios archivos al final
*/

//MAIN SCRIPT
const main = async () => {
    const cliente = new MongoClient("mongodb://localhost:27017");
    
    try {
        await cliente.connect();

        await cliente.db("admin").command({ ping: 1 });
        console.log("Conexión exitosa");

        let db = cliente.db("test");

        let resultado = await db.dropCollection("redes_sociales");
        if (resultado)
            console.log("Se ha borrado la colección redes_sociales");
        else
            console.log("La colección no existía");

        let registros = crearRegistros(100);
        await db.collection("redes_sociales").insertMany(registros);
        console.log("Se han insertado 100 registros");
        
        let cantidad = await db.collection("redes_sociales").find().count();
        console.log(cantidad);

    } catch (e) {
        console.error(e);
    } finally {
        await cliente.close();
    }
}

main();
