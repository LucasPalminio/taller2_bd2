import { paises } from "./coleccionPaises.js";
import { redes_sociales } from "./coleccionRedesSociales.js";

export const crearRegistros = (nroDeRegistros) => {
    const nombresMasculino = ["Lucas", "Victor", "Camilo", "Lorenzo", "Kianush", "Samuel", "Sebastián", "Esteban", "Felipe", "Bayron", "Leandro", "Alfredo", "Alberto", "Javier", "Osvaldo", "Rodrigo", "Matías", "Diego", "Ignacio", "Nicolás", "Luis", "Pablo", "Jorge", "Tomás", "Óscar", "Patricio", "Joaquín", "Pedro", "Manuel", "Gustavo"];
    const nombresFemenino = ["Maria", "Claudia", "Ania", "Trinidad", "Natalia", "Javiera", "Gloria", "Francisca", "Alicia", "Karla", "Francisca", "Millaray", "Josefina", "Camila", "Paula", "Verónica", "María José", "Daniela", "Miriam", "Ingrid", "Esperanza", "Bárbara", "Angélica", "Gabriela", "Marcela", "Soledad", "Belén", "Mónica", "Diana", "Silvia"];
    const apellidos = ["Palminio", "Devia", "Godoy", "Mardones", "Perez", "Sepúlveda", "Sanzana", "Rodriguez", "Cravero", "Muñoz", "Martínez", "Catalán", "Bravo", "Vera", "Sánchez", "Guzmán", "Riquelme", "Reyes", "Fuentes", "Palma", "López", "Molina", "Carmona", "Cifuentes", "Hernández", "Villagrán", "Inostroza", "Bustos", "Medina", "Torres", "García", "Ferrada"];
    
    let resultados = [];

    for (let i = 0; i < nroDeRegistros; i++) {
        let json = {};

        if(enteroEntre(0,1)) {
            json.nombre = eleccionAlAzar(nombresMasculino);
            json.sexo = "M";
        } else {
            json.nombre = eleccionAlAzar(nombresFemenino);
            json.sexo = "F";
        }

        json.apellido = eleccionAlAzar(apellidos);
        json.fechaNacimiento = fechaEntre(new Date(1989, 0, 1), new Date());
        json.fechaRegistro = fechaEntre(json.fechaNacimiento, new Date());
        json.pais = eleccionAlAzar(paises);
        json.redSocial = eleccionAlAzar(redes_sociales);
        
        resultados.push(json);
    }
    return resultados;
}

const enteroEntre = (min, max) => {
    return Math.round(Math.random() * (max - min) + min);
}

const eleccionAlAzar = (arreglo) => {
    return arreglo[Math.floor(Math.random() * arreglo.length)];
}

const fechaEntre = (inicio, final) => {
    var fecha = new Date(+ inicio + Math.random() * (final - inicio));
    return fecha;
}
